![](https://cdn.dribbble.com/users/743744/screenshots/3847043/untitled-1-01.jpg?compress=1&resize=400x300)
# Welcome to the Golang Bootcamp

The idea behind this journey is to not only understand the basics of the language, but also master the fundamental tools to build a fully-working API

We'll divide the trip into three pieces. Each piece will give you tools for the next one.

To get quick advice from the tutors and to monitor applicant's advances, we will host short daily meetings. For more complex issues, tutors can be reached over the chat.

The goal is to complete the Bootcamp in no more than **3 weeks**.

----

## The journey

### Overview

In this section we will just mention and briefly describe each part of our travel and later on we'll expand on what's expected from the applicants.

1. The official Go Tour
2. The In-Memory Database
3. The Shopping Cart REST API

On the Official Go Tour, you will find the basics of this language. Tutors can help you with any questions or issues you may encounter during the daily meetings or even over the chat.

With the In-Memory Database exercise you'l have to use of one of the fundamental parts of the language, Interfaces. You'll also be asked to implement unit-testing but don't worry, read on and be prepared to learn.

With the Shopping Cart REST API you'll use everything you have learned and more in order to complete the final task. Never heard of REST? API? Shopping? Don't worry, we have a section for that.

One last thing... starting from the In-Memory Database program, you will be required to create a repository and issue Merge Requests of the work done; who will approve those? The tutors! So make sure to ask all you need in order to get those approved!

----


### The Official Go Tour

You will quickly notice that Golang has several official guidelines, each with it's own purpose.

For example on Go, you have the Best Practices Guidelines, called Effective Go.
Here's the link for the [Official Effective Go Documentation](https://golang.org/doc/effective_go), keep it handy, you'll need it later.

Of those Official Docs, the one to get you started is the Go Tour.
It will guide you through the basic mechanisms of the language and propose a few small problems for you to solve.

Here's the link for the [Official Tour of Go](https://tour.golang.org/welcome/1)

Once done with that, you can proceed to the next step of the journey.  
![](https://jaxenter.com/wp-content/uploads/2019/09/wizardgopher.png)  


----

### The In-Memory Database

For this one, we'll appeal to your creativity and coding skiils.
You've already finished the Tour so you know what tools the language has to offer.

We want you to create a simple yet effective Memory Database. But let's put the objectives clear in stages.

#### Stage 1
- Develop a simple struct that will be the data model that our In-Memory DB will handle, for example:
```
//Book holds the important data for our books.
// don't use this struct, create your own ;)
type Book struct{
    Author string
    YearPublished int
    Name string
}
```
- Implement the basic CRUD methods to hold the data in memory
_Never heard of CRUD_? it's an acronym that stands for:  
**C**reate : Add a new element to the DB.  
**R**ead: Get an element from the DB _if it exists_.  
**U**pdate: Modify an element inside the DB _if it exists_.  
**D**elete: Erase an element from the DB _if it exists_.  

Ok, but what _if it **does not** exist_? Simple, your methods have to return an error.

- Create an initializer func called `NewDB` that returns an instance that can use all those methods.

- All of the above has to live inside a `db` package that can be imported and used on other programs.

- Create a **sample program** that uses your newly created 'db' package  
This has to be simple, a few Created objects, a few Reads, a few Updates and a few Deletes with console output to see the progress.  

- Read this stage again and see if you caught all the important wording clues, ask the tutors for guidance otherwise.

The **sample program** MUST behave the same each time it's run. Since the DB only persists in memory, that's the expected behaviour.

#### Stage 2
Once you have that `db` package used in that sample program, we'd like you to add unit testing to it.  
Of course if you never heard of unit testing, we'll simplify that by saying the following:  
```
Unit testing helps by making sure the software written in the past is not altered by new features.
Furthermore, it helps to identify misbehaviours, or bugs, before they move out of the development process.
Unit testing ensures that the "little pieces" work correctly, so when assembled into "bigger pieces" we can identify issues easier.  
```

Here's the [Official Documentation of the Go Testing Package](https://golang.org/pkg/testing/), with it you must ensure all your code is tested.  
Make sure you read how to produce coverage reports, an excellent read is in the Golang Blog, specially this article about [The Cover Story](https://blog.golang.org/cover)

To put this clearly with a bullet point:

- Implement unit testing, minimum accepted coverage is 90%.

#### Stage 3

As of now, the data on the DB was lost once the sample program finished.  

For what it holds for you, here are some useful links:  
[Reading Files in Go](https://gobyexample.com/reading-files). Make sure to investigate further on this site, it's useful.  
[Official Documentation of ioutil package](https://golang.org/pkg/io/ioutil/).  
[Official Documentation of os package](https://golang.org/pkg/os/).  

Now We'd like you to add storage persistence to your DB. Yes, saving the data to a file. 
This can be done as simple or as complex as you want, you just have to comply with these bullet points:

- Data of the DB must be retained after the program finishes.
- Modify your DB package to accept a File Name/Location where to store/read the data to/from.
- Fix any broken unit tests.
- Implement the necessary changes to have the coverage level above 80% at least
- The sample program must read the data saved from the previous run. 
- Creations must fail if the element already exists from previous runs.

#### In-Memory/Persistent DB Finished!
![](https://i.pinimg.com/564x/d8/81/6b/d8816ba7c9ded4d6a8f75292f894b19a.jpg)

Great! so now you've developed a simple DB that can store some data.  
You've also implemented unit testing and surely have seen the tests fail when adding the persistence layer (aka saving to a file).  
You've fixed the unit testing, hopefully, and may have found this exercise enlightening as to what comes next.  

----

### The Shopping Cart API

#### Introduction

In order to succeed in this last exercise, you'll need to understand HTTP concepts, REST API Concepts and adittionally Docker concepts.  
For HTTP, you can head over to [The Mozilla Developer Network Docs ](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview), in here you'll find an introduction if you've never heard of HTTP before.  
  
For REST API, there's an excellent guide at [tutorialspoint](https://www.tutorialspoint.com/restful/restful_introduction.htm) that can get you started.  

#### The REST API
This exercise is about a REST API that lets the user:

- Create a Shopping Cart
- Get all available Products
- Add a particular Product to a particular Shopping Cart
- Modify the amount of a particular product in a particular Shopping Cart
- Delete a particular product in a particular Shopping Cart
- Delete all products from a particular Shopping Cart
- Delete a particular Shopping Cart entirely

#### Database

You can choose what databse to use, You'll need to make use of a ORM (Object Relational Mapping) library, we recommend [GORM](https://gorm.io).

#### HTTP Routing

In order to route the requests done to the API you'll need an HTTP Router. We recommend [Gorilla Mux](https://github.com/gorilla/mux) because it's easy to use and the documentation can give you exactly all you need to complete this work.

#### Documentation
Documentation of the endpoints should be done using OpenAPI spec in Swagger format. This step is optional.

#### Containerization
Create a Dockerfile that produces a Container Image such that anyone else can have your REST API running using Docker or any other container orchestration service. This step is optional but we encourage every developer to try their best to achieve it.  

Trying to help in this topic we only provide a link to the [Official Docker Website](https://www.docker.com/get-started), since it's outside of the scope but it's something we believe every backend dev should understand and use.

---

**Note:** Available products to be added to the shopping cart are provided by the following Third-Party API.

| Method   |      URL    | Desc |
|----------|-------------|---   |
| GET | https://bootcamp-products.getsandbox.com/health | Health Endpoint |
| GET | https://bootcamp-products.getsandbox.com/products | To get all available products |
| GET | https://bootcamp-products.getsandbox.com/products/{product_id} | To get an specific product by id. It returns `404` if the _product_id_ is not found |

----

### The End of the journey

At this point, we have to thank you!  
Thank you for participating in this Bootcamp.  
Thank you for taking the time to improve your knowledge.  
Thank you for what we've learned from you.  
And last but not least, thank you for your valuable feedback regarding this journey.  
  
We hope you've enjoyed it, learned something new, improve your coding skills and much more!.  

**Welcome to the Gopher world!!!**  
![](https://www.pngkey.com/png/full/412-4126785_microsoft-oss-conference-database-gopher-golang.png)

  
Psst!, one last tip, head over to [Gopherize Me](https://gopherize.me) to build your own Gopher Style! 
That's it, see you around!  
![](https://img.devrant.com/devrant/rant/r_557673_5ZQ9F.jpg)
